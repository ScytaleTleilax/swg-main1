#!/bin/bash
read -p "***********************************************
This script will cleanup the Chat server logs.
*********************************************** 
Do you want to cleanup chat logs now? (y/n) " response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]]; then

cd /home/swg/swg-main1/chat
rm -rf logs
mkdir logs
fi
echo "Cleanup Chat logs script is complete!"
