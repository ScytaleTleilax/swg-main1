#/bin/bash

awk '/java\.lang/{n=NR+30} n>=NR' output.log  > java.log
zip -9 logz.zip *.log stationapi/logs/* exe/linux/logs/*
rm java.log exe/linux/logs/* output.log

mv logz.zip ~/
