#!/bin/bash

basedir=$PWD
PATH=$PATH:$basedir/build/bin

DBSERVICE=
DBUSERNAME=
DBPASSWORD=
HOSTIP=
CLUSTERNAME=
NODEID=
DSRC_DIR=
DATA_DIR=
MODE=Release

if [ ! -d $basedir/build ]
then
	mkdir $basedir/build
fi

read -p "**********************************************************
This script will add your VM's IP to NGE Server configs  
New configs will be built in /home/swg/swg-main1/exe/linux 
**********************************************************
Do you want to build the config environment now? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then

	# Prompt for configuration environment.
	read -p "Configure environment (local, live, tc, design)? " config_env

	# Make sure the configuration environment exists.
	if [ ! -d $basedir/configs/$config_env ]; then
		echo "Invalid configuration environment."
		exit
	fi

        
	echo "Enter your IP address of your VM on toolbar "
	read HOSTIP

	echo "Enter the DSN for the database connection. i.e. 127.0.0.1 "
	read DBSERVICES

	echo "Enter the database username. i.e. swg "
	read DBUSERNAMES

	echo "Enter the database password i.e swg "
	read DBPASSWORDS

	echo "Enter a name for your galaxy cluster. Use the same name when you imported your swg database. "
	read CLUSTERNAME

	if [ -d $basedir/exe ]; then
		rm -rf $basedir/exe
	fi

	mkdir -p $basedir/exe/linux/logs
	mkdir -p $basedir/exe/shared

	ln -s $basedir/build/bin $basedir/exe/linux/bin

	cp -u $basedir/configs/$config_env/linux/* $basedir/exe/linux
	cp -u $basedir/configs/$config_env/shared/* $basedir/exe/shared

	for filename in $(find $basedir/exe -name '*.cfg'); do
		sed -i -e "s@DBSERVICES@$DBSERVICES@g" -e "s@DBUSERNAMES@$DBUSERNAMES@g" -e "s@DBPASSWORDS@$DBPASSWORDS@g" -e "s@CLUSTERNAME@$CLUSTERNAME@g" -e "s@HOSTIP@$HOSTIP@g" $filename
	done

	#
	# Generate other config files if their template exists.
	#

		# Generate at least 1 node that is the /etc/hosts IP.
		$basedir/utils/build_node_list.sh
fi
echo "Congratulations the build_cfgs script is complete!"
